from random import choice

import toga
from toga.style.pack import *
#from toga.style import Pack
#from toga.constants import COLUMN, ROW

from sqlite_utils import Database
import sqlite3

db = Database(sqlite3.connect("/tmp/dogs.db"))
dogs = db["dogs"]
dogs.insert({
    "name": "Cleo",
    "twitter": "cleopaws",
    "age": 3,
    "is_good_dog": True,
})
dogs.insert({
    "name": "Pancake",
    "twitter": "cakes cakes cakes cakes cakes cakes cakes cakes cakes cakes cakes cakes cakes cakes cakes",
    "age": 2,
    "is_good_dog": False,
})

def build(app):
    data = []
    for dog in db["dogs"].rows:
        # Append to end of table
        data.append(list(dog.values()))

    print(data)

    table = toga.Table(['Name', 'Twitter', 'Age', 'Is Good Dog?'], data=data, style=Pack(width=500, flex=1))

    return table

def main():
    return toga.App('First App', 'org.pybee.helloworld', startup=build)


if __name__ == '__main__':
    main().main_loop()
